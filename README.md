# specs_bundler

specs component and system bundler

```toml
specs_bundler = "0.5"
```

```rust
extern crate specs;
extern crate specs_bundler;


use specs::{DispatcherBuilder, World};
use specs_bundler::{Bundler, Bundle};


#[derive(Default)]
struct MyBundle {
    config: bool
}

impl<'world, 'a, 'b> Bundle<'world, 'a, 'b> for MyBundle {
    type Error = ();

    fn build(
        self,
        bundler: Bundler<'world, 'a, 'b>,
    ) -> Result<Bundler<'world, 'a, 'b>, ()> {
        if self.config {
            Ok(bundler)
        } else {
            Err(())
        }
    }
}

fn main() {
    let mut world = World::new();

    let mut dispatcher = Bundler::new(&mut world, DispatcherBuilder::new())
        .bundle(MyBundle { config: true }).expect("should not be an error")
        .build();

    dispatcher.dispatch(&mut world.res);
}
```
