use super::Bundler;

pub trait Bundle<'world, 'a, 'b> {
    type Error;

    fn bundle(self, Bundler<'world, 'a, 'b>) -> Result<Bundler<'world, 'a, 'b>, Self::Error>;
}
