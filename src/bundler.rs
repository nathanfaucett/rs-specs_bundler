use specs::{Dispatcher, DispatcherBuilder, RunNow, System, World};

use super::Bundle;

pub struct Bundler<'world, 'a, 'b> {
    pub world: &'world mut World,
    pub dispatcher_builder: DispatcherBuilder<'a, 'b>,
}

impl<'world, 'a, 'b> Bundler<'world, 'a, 'b> {
    #[inline(always)]
    pub fn new(world: &'world mut World, dispatcher_builder: DispatcherBuilder<'a, 'b>) -> Self {
        Bundler {
            world: world,
            dispatcher_builder: dispatcher_builder,
        }
    }

    #[inline]
    pub fn bundle<B>(self, bundle: B) -> Result<Self, B::Error>
    where
        B: Bundle<'world, 'a, 'b>,
    {
        bundle.bundle(self)
    }

    #[inline]
    pub fn with<T>(mut self, system: T, name: &str, dep: &[&str]) -> Self
    where
        T: for<'c> System<'c> + Send + 'a,
    {
        self.dispatcher_builder = self.dispatcher_builder.with(system, name, dep);
        self
    }

    #[inline]
    pub fn add<T>(&mut self, system: T, name: &str, dep: &[&str]) -> &mut Self
    where
        T: for<'c> System<'c> + Send + 'a,
    {
        self.dispatcher_builder.add(system, name, dep);
        self
    }

    #[inline]
    pub fn with_thread_local<T>(mut self, system: T) -> Self
    where
        T: for<'c> RunNow<'c> + 'b,
    {
        self.dispatcher_builder = self.dispatcher_builder.with_thread_local(system);
        self
    }

    #[inline]
    pub fn add_thread_local<T>(&mut self, system: T) -> &mut Self
    where
        T: for<'c> RunNow<'c> + 'b,
    {
        self.dispatcher_builder.add_thread_local(system);
        self
    }

    #[inline]
    pub fn with_barrier(mut self) -> Self {
        self.dispatcher_builder = self.dispatcher_builder.with_barrier();
        self
    }

    #[inline]
    pub fn add_barrier(&mut self) -> &mut Self {
        self.dispatcher_builder.add_barrier();
        self
    }

    #[cfg(not(target_os = "emscripten"))]
    #[inline]
    pub fn with_pool(mut self, pool: ::std::sync::Arc<::rayon::ThreadPool>) -> Self {
        self.dispatcher_builder = self.dispatcher_builder.with_pool(pool);
        self
    }

    #[cfg(not(target_os = "emscripten"))]
    #[inline]
    pub fn add_pool(&mut self, pool: ::std::sync::Arc<::rayon::ThreadPool>) -> &mut Self {
        self.dispatcher_builder.add_pool(pool);
        self
    }

    #[inline]
    pub fn print_par_seq(&self) {
        self.dispatcher_builder.print_par_seq()
    }

    #[inline]
    pub fn build(self) -> Dispatcher<'a, 'b> {
        self.dispatcher_builder.build()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use std::fmt;

    struct SimpleError;

    impl fmt::Debug for SimpleError {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "Simple Error")
        }
    }

    struct SimpleBundle {
        should_panic: bool,
    }

    impl SimpleBundle {
        fn new(should_panic: bool) -> Self {
            SimpleBundle {
                should_panic: should_panic,
            }
        }
    }

    impl<'world, 'a, 'b> Bundle<'world, 'a, 'b> for SimpleBundle {
        type Error = SimpleError;

        fn bundle(
            self,
            bundler: Bundler<'world, 'a, 'b>,
        ) -> Result<Bundler<'world, 'a, 'b>, Self::Error> {
            bundler.world.insert([0_usize, 1_usize, 2_usize].to_vec());

            if self.should_panic {
                Err(SimpleError)
            } else {
                Ok(bundler)
            }
        }
    }

    fn bundle<'a, 'b>(
        world: &mut World,
        dispatcher_builder: DispatcherBuilder<'a, 'b>,
        should_panic: bool,
    ) -> Result<Dispatcher<'a, 'b>, SimpleError> {
        Ok(Bundler::new(world, dispatcher_builder)
            .bundle(SimpleBundle::new(should_panic))?
            .build())
    }

    #[test]
    fn test_bundler() {
        let mut world = World::empty();
        let dispatcher_builder = DispatcherBuilder::new();
        let _dispatcher = bundle(&mut world, dispatcher_builder, false).expect("should not fail");

        assert_eq!(
            world.fetch::<Vec<usize>>().as_slice(),
            &[0_usize, 1_usize, 2_usize]
        );
    }

    #[should_panic(expected = "Simple Error")]
    #[test]
    fn test_bundler_fail() {
        let mut world = World::empty();
        let dispatcher_builder = DispatcherBuilder::new();

        let _dispatcher = match bundle(&mut world, dispatcher_builder, true) {
            Ok(dispatcher) => dispatcher,
            Err(e) => panic!("{:?}", e),
        };
    }
}
